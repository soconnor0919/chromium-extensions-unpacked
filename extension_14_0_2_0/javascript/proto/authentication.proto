syntax = "proto3";

package Authentication;
option java_package = "com.keepersecurity.proto";
option java_outer_classname = "Authentication";

enum SupportedLanguage {
  ENGLISH = 0;
  ARABIC = 1;
  BRITISH = 2;
  CHINESE = 3;
  CHINESE_HONG_KONG = 4;
  CHINESE_TAIWAN = 5;
  DUTCH = 6;
  FRENCH = 7;
  GERMAN = 8;
  GREEK = 9;
  HEBREW = 10;
  ITALIAN = 11;
  JAPANESE = 12;
  KOREAN = 13;
  POLISH = 14;
  PORTUGUESE = 15;
  PORTUGUESE_BRAZIL = 16;
  ROMANIAN = 17;
  RUSSIAN = 18;
  SLOVAK = 19;
  SPANISH = 20;
}

enum LoginType {
  NORMAL = 0;
  SSO = 1;
  BIO = 2;
  ALTERNATE = 3;
  OFFLINE = 4;
}

enum DeviceStatus {
  NEED_APPROVAL = 0;
  OK = 1;
  DEVICE_DISABLED = 2;
}

enum LicenseStatus {
  OTHER = 0; // trial for bw
  ACTIVE = 1;
  EXPIRED = 2;
  DISABLED = 3;
}

enum AccountType {
  CONSUMER = 0;
  FAMILY = 1;
  ENTERPRISE = 2;
}

enum SessionTokenType {
  NO_RESTRICTION = 0;
  ACCOUNT_RECOVERY = 1;
  SHARE_ACCOUNT = 2;
  PURCHASE = 3;
}

message ApiRequest {
  bytes encryptedTransmissionKey = 1;
  int32 publicKeyId = 2;
  string locale = 3;
  bytes encryptedPayload = 4;
}

message ApiRequestPayload {
  bytes payload = 1;
  bytes encryptedSessionToken = 2;
  bytes timeToken = 3;
  int32 apiVersion = 4;
}

message Transform {
  bytes key = 1;
  bytes encryptedDeviceToken = 2;
}

message DeviceRequest {
  string clientVersion = 1;
  string deviceName = 2;
}

message AuthRequest {
  string clientVersion = 1;
  string username = 2;
  bytes encryptedDeviceToken = 3;
}

message NewUserMinimumParams {
  int32 minimumIterations = 1;
  repeated string passwordMatchRegex = 2;
  repeated string passwordMatchDescription = 3;
}

message PreLoginRequest {
  AuthRequest authRequest = 1;
  LoginType loginType = 2;
  bytes twoFactorToken = 3; // optional - if supplied, it will be validated if it needs refreshing
}

message LoginRequest {
  AuthRequest authRequest = 1;
  LoginType loginType = 2;
  bytes authenticationHashPrime = 3;
  bytes randomHashKey = 4;
  bytes encryptedTwoFactorToken = 5;
  bytes encryptedBreachWatchToken = 6;
}

message RegistrationRequest {
  AuthRequest authRequest = 1;
  UserAuthRequest userAuthRequest = 2;
  bytes encryptedClientKey = 3; // encrypted with the data key
  bytes encryptedPrivateKey = 4; // encrypted with the data key
  bytes publicKey = 5;
  string verificationCode = 6;

  bytes deprecatedAuthHashHash = 7; // these will be ignored once the v2 clients are obsolete
  bytes deprecatedEncryptedClientKey = 8;
  bytes deprecatedEncryptedPrivateKey = 9;
  bytes deprecatedEncryptionParams = 10;
}

message DeviceResponse {
  bytes encryptedDeviceToken = 1;
  DeviceStatus status = 2;
}

message Salt {
  int32 iterations = 1;
  bytes salt = 2;
  int32 algorithm = 3;
  bytes uid = 4;
  string name=5;
}

message TwoFactorChannel {
  int32 type = 1;
}

message PreLoginResponse {
  DeviceStatus status = 1;
  repeated Salt salt = 2;
  repeated TwoFactorChannel twoFactorChannel = 3;
}

message LoginResponse {
  bytes encryrptedSessionToken = 1;
  License vault = 2;
  License chat = 3;
  License storage = 4;
  License breachWatch = 5;
  AccountType accountType = 6;
  bytes encryptedDAT = 7;
  bytes encryptedPAT = 8;
  bytes encryptedEAT = 9;
  bytes encryptedDataKey = 10;
  repeated SessionTokenType sessionTokenType = 11;
}

message License {
   int64 created = 1;
   int64 expiration = 2;
   LicenseStatus licenseStatus = 3;
   bool paid = 4;
   string message = 5;
}

message OwnerlessRecord {
  bytes recordUid = 1; // used in get request, set request, set response
  bytes recordKey = 2; // used in set request
  int32 status = 3; // used in set response
}

message OwnerlessRecords {
  repeated OwnerlessRecord ownerlessRecord = 1;
}

message UserAuthRequest {
  bytes uid = 1;
  bytes salt = 2;
  int32 iterations = 3;
  bytes encryptedClientKey = 4;
  bytes authHash = 5;
  bytes encryptedDataKey = 6;
  LoginType loginType = 7;
  string name = 8;
  int32 algorithm = 9;
}

message UidRequest {
  repeated bytes uid = 1;
}

message DeviceClientVersionUpdateRequest {
  bytes encryptedDeviceToken = 1;
  string clientVersion = 2;
}

message ConvertUserToV3Request {
   AuthRequest authRequest = 1;
   UserAuthRequest userAuthRequest = 2;
   bytes encryptedClientKey = 3; // gcm encrypted with the data key
   bytes encryptedPrivateKey = 4; // gcm encrypted with the data key
   bytes publicKey = 5;
   // TODO add alternate passwords
}

message RevisionResponse {
  int64 revision = 1;
}

message ChangeEmailRequest {
    string newEmail = 1;
}

message ChangeEmailResponse {
    bytes encryptedChangeEmailToken = 1;
}
